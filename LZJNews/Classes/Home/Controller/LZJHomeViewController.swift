//
//  LZJHomeViewController.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class LZJHomeViewController: UIViewController {
    
    fileprivate let newsTitleTable = NewsTitleTable()
    override func viewDidLoad() {
        super.viewDidLoad()

       view.backgroundColor = UIColor.white
        NetworkTool.loadHomeNewsTitleData { (titles) in
            
            self.newsTitleTable.inserts(titles)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
