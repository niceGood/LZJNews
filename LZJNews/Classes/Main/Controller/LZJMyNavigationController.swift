//
//  LZJMyNavigationController.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class LZJMyNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "lefterbackicon_titlebar_24x24_"), style: .plain, target: self, action: #selector(navigationBack))
        }
        super.pushViewController(viewController, animated: true)
        
    }
    /// 返回上一控制器
    @objc private func navigationBack() {
        popViewController(animated: true)
    }

    

}
