//
//  LZJMyTabBarController.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class LZJMyTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabbar = UITabBar.appearance()
        tabbar.tintColor = UIColor(red: 245 / 255.0, green: 90 / 255.0, blue: 93 / 255.0, alpha: 1.0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveDayOrNightButtonClicked), name: NSNotification.Name(rawValue: "dayOrNightButtonClicked"), object: nil)

        addChildViewControllers()
       
    }
    
    @objc func receiveDayOrNightButtonClicked(Notification:NSNotification) {
        
        let selected = Notification.object as! Bool
        if selected{//夜间
            
            for childController in childViewControllers{
                switch childController.title! {
                case "首页":
                    setNightChildController(controller: childController, imageName: "home")
                case "视频":
                    setNightChildController(controller: childController, imageName: "video")
                case "小视频":
                    setNightChildController(controller: childController, imageName: "huoshan")
                case "未登录":
                    setNightChildController(controller: childController, imageName: "no_login")
                default:
                    break
                }
            }
        }else{//日间
            for childController in childViewControllers{
            switch childController.title! {
            case "首页":
                setDayChildController(controller: childController, imageName: "home")
            case "视频":
                setDayChildController(controller: childController, imageName: "video")
            case "小视频":
                setDayChildController(controller: childController, imageName: "huoshan")
            case "未登录":
                setDayChildController(controller: childController, imageName: "no_login")
            default:
                break
            }
            }
        }
        
    }
    
    /// 设置夜间控制器
    private func setNightChildController(controller: UIViewController, imageName: String) {
        controller.tabBarItem.image = UIImage(named: imageName + "_tabbar_night_32x32_")
        controller.tabBarItem.selectedImage = UIImage(named: imageName + "_tabbar_press_night_32x32_")
    }
    
    /// 设置日间控制器
    private func setDayChildController(controller: UIViewController, imageName: String) {
        controller.tabBarItem.image = UIImage(named: imageName + "_tabbar_32x32_")
        controller.tabBarItem.selectedImage = UIImage(named: imageName + "_tabbar_press_32x32_")
    }
    
    /// 添加子控制器
    private func addChildViewControllers() {
        setChildViewController(LZJHomeViewController(), title: "首页", imageName: "home")
        setChildViewController(LZJVideoViewController(), title: "视频", imageName: "video")
        setChildViewController(LZJHuoshanViewController(), title: "小视频", imageName: "huoshan")
        setChildViewController(LZJMineViewController(), title: "未登录", imageName: "no_login")
        // tabBar 是 readonly 属性，不能直接修改，利用 KVC 把 readonly 属性的权限改过来
        setValue(LZJMyTabBar(), forKey: "tabBar")
    }
    
    /// 初始化子控制器
    private func setChildViewController(_ childController: UIViewController, title: String, imageName: String) {
        // 设置 tabbar 文字和图片
        if UserDefaults.standard.bool(forKey: isNight) {
            setNightChildController(controller: childController, imageName: imageName)
        } else {
            setDayChildController(controller: childController, imageName: imageName)
        }
        childController.title = title
        // 添加导航控制器为 TabBarController 的子控制器
        let navVc = LZJMyNavigationController(rootViewController: childController)
        addChildViewController(navVc)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
  

   

}
