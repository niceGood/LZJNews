//
//  UITableView+Extension.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

extension UITableView {
    
    /// 注册 cell 的方法
    func lzj_registerCell<T:UITableViewCell>(cell:T.Type) where T:RegisterCellOrNib  {
        if let nib = T.nib {
            register(nib, forCellReuseIdentifier: T.identifier)
        }else{
            register(cell, forCellReuseIdentifier: T.identifier)
        }
    }
    /// 从缓存池池出队已经存在的 cell
    func lzj_dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T where T: RegisterCellOrNib {
        return dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
}
