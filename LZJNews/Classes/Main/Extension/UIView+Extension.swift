//
//  UIView+Extension.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit
protocol RegisterCellOrNib {}
extension RegisterCellOrNib {
    
    static var identifier:String {
        
        return "\(self)"
    }
    static var nib:UINib? {
        
        return UINib(nibName: "\(self)", bundle: nil)
    }
    
    
    
}
