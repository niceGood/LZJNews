//
//  SQLiteManager.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/6.
//  Copyright © 2018年 李志军. All rights reserved.
//

import Foundation
import SQLite

struct SQLiteManager {
    var database:Connection!
    init() {
        do {
            let path = NSHomeDirectory() + "/Documents/news.sqlite3"
            database = try Connection(path)
            
        } catch  {
            print(error)
        }
    }
    
}
var category: String = ""
var tip_new: Int = 0
var default_add: Int = 0
var web_url: String = ""
var concern_id: String = ""
var icon_url: String = ""
var flags: Int = 0
var type: Int = 0
var name: String = ""

var selected: Bool = true

/// 首页新闻分类的标题数据表
struct NewsTitleTable {
    ///数据库管理者
   fileprivate let sqlManager = SQLiteManager()
    ///新闻标题 建表
   fileprivate let news_title = Table("news_title")
    ///表字段
     let id = Expression<Int64>("id")
     let category = Expression<String>("category")
     let tip_new = Expression<Int64>("tip_new")
     let default_add = Expression<Int64>("default_add")
     let web_url = Expression<String>("web_url")
     let concern_id = Expression<String>("concern_id")
     let icon_url = Expression<String>("icon_url")
     let flags = Expression<Int64>("flags")
     let type = Expression<Int64>("type")
     let name = Expression<String>("name")
     let selected = Expression<Bool>("selected")
    
    init() {
        do {
            
            try sqlManager.database.run(news_title.create(ifNotExists: true, block: { t in
                t.column(id, primaryKey: true)
                t.column(category)
                t.column(tip_new)
                t.column(default_add)
                t.column(web_url)
                t.column(concern_id)
                t.column(icon_url)
                t.column(flags)
                t.column(type)
                t.column(name)
                t.column(selected)
            }))
        } catch  {
            print(error)
        }
    }
    
    ///插入一组数据
    func inserts(_ titles:[HomeNewsTitle]) {
        for title in titles {
            
            insert(title)
        }
    }
    
    
    ///插入一条数据
    func insert(_ title:HomeNewsTitle) {
        
        if !exist(title) {
            
            let insert = news_title.insert(category <- title.category, tip_new <- Int64(title.tip_new), default_add <- Int64(title.default_add), concern_id <- title.concern_id, web_url <- title.web_url, icon_url <- title.icon_url, flags <- Int64(title.flags), type <- Int64(title.type), name <- title.name, selected <- title.selected)
            
            do {
              try sqlManager.database.run(insert)
                
            } catch  {
                print(error)
            }
            
            
        }
        
    }
    
     /// 判断数据库中某一条数据是否存在
    func exist(_ title:HomeNewsTitle) -> Bool {
        
        let title = news_title.filter(category == title.category)
        do {
            let count = try sqlManager.database.scalar(title.count)
            return count != 0
            
        } catch  {
            print(error)
        }
        
        
        return false
    }
    
    func selectAll() -> [HomeNewsTitle] {
        
        var allTitles = [HomeNewsTitle]()
       
        do {
            for title in try sqlManager.database.prepare(news_title) {
                let newsTitle = HomeNewsTitle(category: title[category], tip_new: Int(title[tip_new]), default_add: Int(title[default_add]), web_url: title[web_url], concern_id: title[concern_id], icon_url: title[icon_url], flags: Int(title[flags]), type: Int(title[type]), name: title[name], selected: title[selected])
                allTitles.append(newsTitle)
                
            }
            
            return allTitles
            
        } catch  {
            print(error)
        }
        return []
    }
    
    /// 更新数据
    func update(_ newTitle:HomeNewsTitle) {
        do {
             // 取出数据库中数据
            let title = news_title.filter(category == newTitle.category)
            try sqlManager.database.run(title.update(selected <- newTitle.selected))
            
        } catch  {
            print(error)
        }
    }
    
    
}
