//
//  LZJMyTabBar.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class LZJMyTabBar: UITabBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(publishButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var publishButton:UIButton = {
        let publishButton = UIButton(type: .custom)
        publishButton.setBackgroundImage(UIImage(named: "feed_publish_44x44_"), for: .normal)
        publishButton.setBackgroundImage(UIImage(named: "feed_publish_press_44x44_"), for: .selected)
        publishButton.sizeToFit()
        return publishButton
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // 当前 tabbar 的宽度和高度
        let width = frame.width
        let height:CGFloat = 49
        publishButton.center = CGPoint(x: width*0.5, y: height*0.5 - 7)
         // 设置其他按钮的 frame
        let buttonW = width * 0.2
        let buttonH:CGFloat = height
        let buttonY:CGFloat = 0
        var index = 0
        for button in subviews {
           if !button.isKind(of: NSClassFromString("UITabBarButton")!) { continue }
            let buttonX = buttonW * (index > 1 ? (CGFloat)(index + 1): CGFloat(index))
            button.frame = CGRect(x: buttonX, y: buttonY, width: buttonW, height: buttonH)
            index += 1
        }
        
        
        
    }
    

}
