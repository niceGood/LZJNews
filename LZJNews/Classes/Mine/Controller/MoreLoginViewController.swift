//
//  MoreLoginViewController.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/1.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit
import IBAnimatable


class MoreLoginViewController: AnimatableModalViewController {

    @IBOutlet weak var loginCloseButton: UIButton!
     /// 顶部标题
    @IBOutlet weak var topLabel: UILabel!
    ///手机号view
    @IBOutlet weak var mobileView: AnimatableView!
    ///验证码view
    @IBOutlet weak var passwrodView: AnimatableView!
    ///发送验证码按钮
    @IBOutlet weak var sendVerifyButton: UIButton!
    ///手机号 输入框
    @IBOutlet weak var mobileTextField: UITextField!
    ///找回密码按钮
    @IBOutlet weak var findPasswordButton: UIButton!
    ///密码输入框
    @IBOutlet weak var passwordtextField: UITextField!
    ///未注册label
    @IBOutlet weak var middleTipLabel: UILabel!
    ///进入头条按钮
    @IBOutlet weak var enterTouTiaoButton: AnimatableButton!
    ///阅读条款
    @IBOutlet weak var readLabel: UILabel!
    ///阅读选中按钮
    @IBOutlet weak var readButton: UIButton!
    ///账号密码登录
    @IBOutlet weak var loginModeButton: UIButton!
    ///微信登录
    @IBOutlet weak var wechatLoginButton: UIButton!
    ///qq登录
    @IBOutlet weak var qqLoginButton: UIButton!
    ///天翼登录
    @IBOutlet weak var tianyiLoginButton: UIButton!
    ///邮箱登录
    @IBOutlet weak var mailLoginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginModeButton.setTitle("免密码登录", for: .selected)
        view.theme_backgroundColor = "colors.cellBackgroundColor"
        topLabel.theme_textColor = "colors.black"
        middleTipLabel.theme_textColor = "colors.cellRightTextColor"
        readLabel.theme_textColor = "colors.black"
        enterTouTiaoButton.theme_backgroundColor = "colors.enterToutiaoBackgroundColor"
        enterTouTiaoButton.theme_setTitleColor("colors.enterToutiaoTextColor", forState: .normal)
        readButton.theme_setImage("images.loginReadButton", forState: .selected)
        readButton.theme_setImage("images.loginReadButtonSelected", forState: .normal)
        mobileView.theme_backgroundColor = "colors.loginMobileViewBackgroundColor"
        passwrodView.theme_backgroundColor = "colors.loginMobileViewBackgroundColor"
        loginCloseButton.theme_setImage("images.loginCloseButtonImage", forState: .normal)
        wechatLoginButton.theme_setImage("images.moreLoginWechatButton", forState: .normal)
        qqLoginButton.theme_setImage("images.moreLoginQQButton", forState: .normal)
        tianyiLoginButton.theme_setImage("images.moreLoginTianyiButton", forState: .normal)
        mailLoginButton.theme_setImage("images.moreLoginMailButton", forState: .normal)
       
        
    }
///账号密码登录按钮点击
 
    @IBAction func loginModeButtonCicked(_ sender: UIButton) {
        loginModeButton.isSelected = !sender.isSelected
       
        middleTipLabel.isHidden = sender.isSelected
        passwordtextField.placeholder = sender.isSelected ? "密码" : "请输入验证码"
        topLabel.text = sender.isSelected ? "帐号密码登录" : "登录你的头条，精彩永不消失"
    }
    
    
    @IBAction func readButtonClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    /// 关闭按钮点击
    @IBAction func moreLoginColseButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }


}
