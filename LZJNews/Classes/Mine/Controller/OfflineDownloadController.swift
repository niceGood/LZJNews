//
//  OfflineDownloadController.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/6.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit
import SnapKit

class OfflineDownloadController: UITableViewController {

    // 标题数组
    fileprivate var titles = [HomeNewsTitle]()
    // 标题数据表
    fileprivate let newsTitleTable = NewsTitleTable()
    override func viewDidLoad() {
        super.viewDidLoad()

       tableView.lzj_registerCell(cell: OfflineDownlaodCell.self)
        tableView.rowHeight = 44
        tableView.theme_separatorColor = "colors.separatorViewColor"
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView.theme_backgroundColor = "colors.tableViewBackgroundColor"
        
        titles = newsTitleTable.selectAll()
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return titles.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.lzj_dequeueReusableCell(indexPath: indexPath) as OfflineDownlaodCell
         let newsTitle = titles[indexPath.row]
        cell.textLabel?.text = newsTitle.name
        cell.rightImageView.theme_image = newsTitle.selected ? "images.air_download_option_press" : "images.air_download_option"
        
        return cell
        
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 44)
        view.theme_backgroundColor = "colors.tableViewBackgroundColor"
        let label = UILabel()
        label.theme_textColor = "colors.black"
        label.text = "我的频道"
        let separatorView = UIView()
        separatorView.theme_backgroundColor = "colors.separatorViewColor"
        view.addSubview(label)
        view.addSubview(separatorView)
        label.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(20)
        }
        separatorView.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: screenWidth, height: 1))
            make.leading.trailing.bottom.equalToSuperview()
        }
        return view
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var homeNewsTitle = titles[indexPath.row]
        // 取反
        homeNewsTitle.selected = !homeNewsTitle.selected
        let cell = tableView.cellForRow(at: indexPath) as! OfflineDownlaodCell
        cell.rightImageView.theme_image = homeNewsTitle.selected ? "images.air_download_option_press" : "images.air_download_option"
        titles[indexPath.row] = homeNewsTitle
        
        newsTitleTable.update(homeNewsTitle)
        
        tableView.reloadRows(at: [indexPath], with: .none)
        
        
    }
    

    

}
