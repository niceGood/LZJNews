//
//  SettingModel.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/5.
//  Copyright © 2018年 李志军. All rights reserved.
//

import Foundation
import HandyJSON
struct SettingModel:HandyJSON {
    var title: String = ""
    var subtitle: String = ""
    var rightTitle: String = ""
    var isHiddenSubtitle: Bool = false
    var isHiddenRightTitle: Bool = false
    var isHiddenSwitch: Bool = false
    var isHiddenRightArraw: Bool = false
}
