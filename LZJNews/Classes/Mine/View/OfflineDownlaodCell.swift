//
//  OfflineDownlaodCell.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/6.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class OfflineDownlaodCell: UITableViewCell,RegisterCellOrNib {

    /// 标题
    @IBOutlet weak var titleLabel: UILabel!
    /// 勾选图片
    @IBOutlet weak var rightImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
