//
//  SettingCell.swift
//  LZJNews
//
//  Created by kingdom on 2018/6/5.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell,RegisterCellOrNib {

    var setting:SettingModel?{
        didSet{
            titleLabel.text = setting?.title
            subtitleLabel.text = setting?.subtitle
            rightTitleLabel.text = setting?.rightTitle
            arrowImageView.isHidden = setting!.isHiddenRightArraw
            switchView.isHidden = setting!.isHiddenSwitch
            if !(setting?.isHiddenSubtitle)! {
                subtitleLabelHeight.constant = 20
                layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var subtitleLabelHeight: NSLayoutConstraint!
    /// 标题
    @IBOutlet weak var titleLabel: UILabel!
    /// 副标题
    @IBOutlet weak var subtitleLabel: UILabel!
    /// 右边标题
    @IBOutlet weak var rightTitleLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var switchView: UISwitch!
    
    @IBOutlet weak var bottomLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
