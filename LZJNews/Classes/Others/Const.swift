//
//  Const.swift
//  LZJNews
//
//  Created by kingdom on 2018/5/30.
//  Copyright © 2018年 李志军. All rights reserved.
//

import UIKit

///屏幕宽度
let screenWidth = UIScreen.main.bounds.width
///屏幕高度
let screenHeight = UIScreen.main.bounds.height
/// 服务器地址
//let BASE_URL = "http://lf.snssdk.com"
//let BASE_URL = "http://ib.snssdk.com"
let BASE_URL = "https://is.snssdk.com"

let device_id: Int = 6096495334
let iid: Int = 5034850950
let kMyHeaderViewHeight: CGFloat = 280
let isNight = "isNight"
let isIPhoneX: Bool = screenHeight == 812 ? true : false
